#region File Description
//-----------------------------------------------------------------------------
// GameplayScreen.cs
//
// Microsoft XNA Community Game Platform
// Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using System;
using System.Threading;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using _80_apple_juice;
using _80_apple_juice.AssetManager;
using System.Collections.Generic;
#endregion

namespace GameStateManagement
{
    /// <summary>
    /// This screen implements the actual game logic. It is just a
    /// placeholder to get the idea across: you'll probably want to
    /// put some more interesting gameplay in here!
    /// </summary>
    class GameplayScreen : GameScreen
    {
        #region Fields
        ContentManager content;
        SpriteBatch spriteBatch;
        Camera Camera;
        Platform[] platformArray;
        HealingPlatform Healing;
        AttackPlatform Attack;
        Shoujo player1;
        Shoujo player2;
        Random rand;
        float x;
        float y;
        float z;
        public Vector3 playerPos;
        public Vector3 enemyPos;
        public float rotationY;
        public float ErotationY;
        public bool yourturn;
        public bool enemyturn;
        #endregion

        #region Initialization


        /// <summary>
        /// Constructor.
        /// </summary>
        public GameplayScreen()
        {
            TransitionOnTime = TimeSpan.FromSeconds(1.5);
            TransitionOffTime = TimeSpan.FromSeconds(0.5);
            Camera = new Camera();
            platformArray = new Platform[16];
            Healing = new HealingPlatform();
            Attack = new AttackPlatform();
            player1 = new Shoujo();
            player2 = new Shoujo();
            for (int i = 0; i < platformArray.Length; i++)
            {
                platformArray[i] = new Platform();
            }

            x = 0.5f;
            y = 0.2f;
            z = 0.5f;
            yourturn = true;
            enemyturn = false;
            rand = new Random();
        }


        /// <summary>
        /// Load graphics content for the game.
        /// </summary>
        public override void LoadContent()
        {
            if (content == null)
                content = new ContentManager(ScreenManager.Game.Services, "Content");
            // Load Content HERE
            spriteBatch = new SpriteBatch(ScreenManager.GraphicsDevice);

            player1.LoadContent(content, ScreenManager.GraphicsDevice);


            for (int i = 0; i < platformArray.Length; i++)
            {
                platformArray[i].LoadContent(content, ScreenManager.GraphicsDevice);
            }

            playerPos = new Vector3(x, y, z);
            enemyPos = new Vector3(19.5f,0.2f,19.5f);
            ErotationY = 3.14f;
            rotationY = 0;
            // once the load has finished, we use ResetElapsedTime to tell the game's
            // timing mechanism that we have just finished a very long frame, and that
            // it should not try to catch up.
            ScreenManager.Game.ResetElapsedTime();
        }


        /// <summary>
        /// Unload graphics content used by the game.
        /// </summary>
        public override void UnloadContent()
        {
            content.Unload();
        }


        #endregion

        #region Update and Draw


        /// <summary>
        /// Updates the state of the game. This method checks the GameScreen.IsActive
        /// property, so the game will stop updating when the pause menu is active,
        /// or if you tab away to a different application.
        /// </summary>
        public override void Update(GameTime gameTime, bool otherScreenHasFocus,
                                                       bool coveredByOtherScreen)
        {
            base.Update(gameTime, otherScreenHasFocus, false);

            if (IsActive)
            {
                /////////////////player and enemy turning is turning of DIRECTION, not taking turns///////////////////////

                    /////////////////////////////////player turning start/////////////////////////////////////
                    //playerPos.Z += 0.1f;
                    if (playerPos.Z >= 19.5f && playerPos.X <= 19.5)
                    {
                        playerPos.Z = 19.5f;
                        //playerPos.Z += 0;
                        //playerPos.X += 0.1f;
                        rotationY = 1.57f;
                    }
                    if (playerPos.X >= 19.5f && playerPos.Z <= 19.5)
                    {
                        playerPos.X = 19.5f;
                        //playerPos.X += 0;
                        //playerPos.Z -= 0.2f;
                        rotationY = 3.14f;
     
                    }
                   
                
                    //playerPos.Z -= 0.1f;
                    if (playerPos.Z <= 0 && playerPos.X <= 19.5)
                    {
                        playerPos.Z = 0.5f;
                        //playerPos.Z -= 0.1f;
                        //playerPos.X -= 0.2f;
                        rotationY = 4.71f;
                       
                    }
                    if (playerPos.X <= 0 && playerPos.Z <= 19.5)
                    {
                        playerPos.X = 0.5f;
                        //playerPos.X -= 0;
                        //playerPos.Z += 0.1f;
                        rotationY = 6.28f;
                        
                    }


                    /////////////////////////////player turning end//////////////////////////////////

                   /////////////////////////////enemy turning start//////////////////////////////////
                    if (enemyPos.Z >= 19.5f && enemyPos.X <= 19.5)
                    {
                        enemyPos.Z = 19.5f;
                        //playerPos.Z += 0;
                        //playerPos.X += 0.1f;
                        ErotationY = 1.57f;
                    }
                    if (enemyPos.X >= 19.5f && enemyPos.Z <= 19.5)
                    {
                        enemyPos.X = 19.5f;
                        //playerPos.X += 0;
                        //playerPos.Z -= 0.2f;
                        ErotationY = 3.14f;

                    }


                    //playerPos.Z -= 0.1f;
                    if (enemyPos.Z <= 0 && enemyPos.X <= 19.5)
                    {
                        enemyPos.Z = 0.5f;
                        //playerPos.Z -= 0.1f;
                        //playerPos.X -= 0.2f;
                        ErotationY = 4.71f;

                    }
                    if (enemyPos.X <= 0 && enemyPos.Z <= 19.5)
                    {
                        enemyPos.X = 0.5f;
                        //playerPos.X -= 0;
                        //playerPos.Z += 0.1f;
                        ErotationY = 6.28f;

                    }
                    //////////////////////////////////////enemy turning end/////////////////////////////////////

                    //update platforms
                    Healing.Update(player1, playerPos, new Vector3(0, 0, 19));
                    Attack.Update(player1, playerPos, new Vector3(19, 0, 0));
                    Healing.Update(player2, enemyPos, new Vector3(0, 0, 19));
                    Attack.Update(player2, enemyPos, new Vector3(19, 0, 0));

                    if (player1.health == 0 && player2.health > 0) 
                    {
                        //player 2 wins
                    }
                    else if (player1.health > 0 && player2.health == 0) 
                    {
                        //player 1 wins
                    }
                    if (player1.health == 0 && player2.health == 0) 
                    {
                        //draw
                    }
                    
                // Add Update Stuff HERE
                Camera.Update();
            }
        }


        /// <summary>
        /// Lets the game respond to player input. Unlike the Update method,
        /// this will only be called when the gameplay screen is active.
        /// </summary>
        public override void HandleInput(InputState input)
        {
            if (input == null)
                throw new ArgumentNullException("input");
            // Look up inputs for the active player profile.
            int playerIndex = (int)ControllingPlayer.Value;
            KeyboardState keyboardState = input.CurrentKeyboardStates[playerIndex];
            GamePadState gamePadState = input.CurrentGamePadStates[playerIndex];
            // The game pauses either if the user presses the pause button, or if they unplug the
            // active gamepad. This requires us to keep track of whether a gamepad was ever plugged
            // in, because we don't want to pause on PC if they are playing with a keyboard and have
            // no gamepad at all!
            bool gamePadDisconnected = !gamePadState.IsConnected &&
            input.GamePadWasConnected[playerIndex];
            ///////////////////////////////debug movement/////////////////////////////////////
            if (keyboardState.IsKeyDown(Keys.Up))
            {
                playerPos.Z += 1;
                if (playerPos.Z >= 19.5f)
                {
                    playerPos.Z = 19.5f;
                }
            }
            if (keyboardState.IsKeyDown(Keys.Left))
            {
                playerPos.X += 1;
                if (playerPos.X >= 20f)
                {
                    playerPos.X = 20f;
                }
            }
            if (keyboardState.IsKeyDown(Keys.Down))
            {
                playerPos.Z -= 1;
                if (playerPos.Z <= 0)
                {
                    playerPos.Z = 0;
                }
            }
            if (keyboardState.IsKeyDown(Keys.Right))
            {
                playerPos.X -= 1;
                if (playerPos.X <= 0)
                {
                    playerPos.X = 0;
                }
            }
            //////////////////////debug movement end//////////////////////////////////
            //////////////////////////player turn//////////////////////////////////////
            if (keyboardState.IsKeyDown(Keys.F1) && enemyturn == false && yourturn == true) 
            {
                float result = (float)OneDice();
                if (playerPos.Z <= 19.5 && playerPos.X == 0.5)
                {
                    
                    if (result > 4)
                    {
                        playerPos.Z = 19.5f;
                        playerPos.X += ((result - 4) * 5);
                    }
                    else
                    {
                        playerPos.Z += (result * 5);
                    }
                }
                else if (playerPos.Z == 19.5 && playerPos.X <= 19.5) 
                {
                    
                    if (result > 4)
                    {
                        playerPos.X = 19.5f;
                        playerPos.Z -= ((result - 4) * 5);
                    }
                    else
                    {
                        playerPos.X += (result * 5);
                    }
                }
                else if (playerPos.X == 19.5 && playerPos.Z >= 0.5) 
                {
                    
                    
                    if (result > 4)
                    {
                        playerPos.Z = 0.5f;
                        playerPos.X -= ((result - 4) * 5);
                    }
                    else
                    {
                        playerPos.Z -= (result * 5);
                    }
                }
                else if (playerPos.Z == 0.5 && playerPos.X >= 0.5) 
                {
                    
                    
                    if (result > 4)
                    {
                        playerPos.X = 0.5f;
                        playerPos.Z += ((result - 4) * 5);
                    }
                    else 
                    {
                        playerPos.X -= (result * 5);
                    }
                }
                yourturn = false;
                enemyturn = true;
            }
            ////////////////////////enemy turn////////////////////////////
            if (keyboardState.IsKeyDown(Keys.F2) && yourturn == false && enemyturn == true)
            {
                float result = (float)OneDice();
                if (enemyPos.Z <= 19.5 && enemyPos.X == 0.5)
                {

                    if (result > 4)
                    {
                        enemyPos.Z = 19.5f;
                        enemyPos.X += ((result - 4) * 5);
                    }
                    else
                    {
                        enemyPos.Z += (result * 5);
                    }
                }
                else if (enemyPos.Z == 19.5 && enemyPos.X <= 19.5)
                {

                    if (result > 4)
                    {
                        enemyPos.X = 19.5f;
                        enemyPos.Z -= ((result - 4) * 5);
                    }
                    else
                    {
                        enemyPos.X += (result * 5);
                    }
                }
                else if (enemyPos.X == 19.5 && enemyPos.Z >= 0.5)
                {


                    if (result > 4)
                    {
                        enemyPos.Z = 0.5f;
                        enemyPos.X -= ((result - 4) * 5);
                    }
                    else
                    {
                        enemyPos.Z -= (result * 5);
                    }
                }
                else if (enemyPos.Z == 0.5 && enemyPos.X >= 0.5)
                {


                    if (result > 4)
                    {
                        enemyPos.X = 0.5f;
                        enemyPos.Z += ((result - 4) * 5);
                    }
                    else
                    {
                        enemyPos.X -= (result * 5);
                    }
                }
                enemyturn = false;
                yourturn = true;
            }
            if (input.IsPauseGame(ControllingPlayer) || gamePadDisconnected)
            {
                ScreenManager.AddScreen(new PauseMenuScreen(), ControllingPlayer);
                return;
            }
            // Add Input Statements HERE

        }

        public int OneDice()
        {
            int dice = 0;
            return dice = rand.Next(1,6);
        }
        /// <summary>
        /// Draws the gameplay screen.
        /// </summary>
        public override void Draw(GameTime gameTime)
        {
            // This game has a Deep SkyBlue background. Why? Because Cornflower Blue is ugly.
            ScreenManager.GraphicsDevice.Clear(ClearOptions.Target,Color.DeepSkyBlue, 0, 0);
            SpriteBatch spriteBatch = ScreenManager.SpriteBatch;
            spriteBatch.Begin();
            
            spriteBatch.End();

            //Add draw code for platforms between here
            platformArray[00].Draw(ScreenManager.GraphicsDevice, Camera, new Vector3(00, 00, 00));
            platformArray[01].Draw(ScreenManager.GraphicsDevice, Camera, new Vector3(05, 00, 00));
            platformArray[02].Draw(ScreenManager.GraphicsDevice, Camera, new Vector3(10, 00, 00));
            platformArray[03].Draw(ScreenManager.GraphicsDevice, Camera, new Vector3(15, 00, 00));
            platformArray[04].Draw(ScreenManager.GraphicsDevice, Camera, new Vector3(20, 00, 00));
            platformArray[05].Draw(ScreenManager.GraphicsDevice, Camera, new Vector3(20, 00, 05));
            platformArray[06].Draw(ScreenManager.GraphicsDevice, Camera, new Vector3(20, 00, 10));
            platformArray[07].Draw(ScreenManager.GraphicsDevice, Camera, new Vector3(20, 00, 15));
            platformArray[08].Draw(ScreenManager.GraphicsDevice, Camera, new Vector3(20, 00, 20));
            platformArray[09].Draw(ScreenManager.GraphicsDevice, Camera, new Vector3(15, 00, 20));
            platformArray[10].Draw(ScreenManager.GraphicsDevice, Camera, new Vector3(10, 00, 20));
            platformArray[11].Draw(ScreenManager.GraphicsDevice, Camera, new Vector3(05, 00, 20));
            platformArray[12].Draw(ScreenManager.GraphicsDevice, Camera, new Vector3(00, 00, 20));
            platformArray[13].Draw(ScreenManager.GraphicsDevice, Camera, new Vector3(00, 00, 15));
            platformArray[14].Draw(ScreenManager.GraphicsDevice, Camera, new Vector3(00, 00, 10));
            platformArray[15].Draw(ScreenManager.GraphicsDevice, Camera, new Vector3(00, 00, 05));

            //and here

            player1.Draw(ScreenManager.GraphicsDevice, Camera, playerPos, rotationY);
            player2.Draw(ScreenManager.GraphicsDevice, Camera, enemyPos, ErotationY);

            ScreenManager.GraphicsDevice.BlendState = BlendState.Opaque;
            ScreenManager.GraphicsDevice.DepthStencilState = DepthStencilState.Default;
            ScreenManager.GraphicsDevice.SamplerStates[0] = SamplerState.LinearWrap;

            
            if (TransitionPosition > 0)
                ScreenManager.FadeBackBufferToBlack(1 - TransitionAlpha);
        }


        
        #endregion
    }
}
