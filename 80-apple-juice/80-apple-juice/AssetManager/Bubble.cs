﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _80_apple_juice.AssetManager
{
    public class Bubble
    {
        int bubbleSpeed;
        Rectangle bubbleRect;
        Texture2D bubbleTexture;
        Random random;

        public Bubble()
        {
            random = new Random(System.DateTime.Now.Millisecond);
            bubbleRect = new Rectangle(random.Next(1280), random.Next(720), 200, 200);
            bubbleSpeed = random.Next(1,10);
        }

        public void LoadContent(ContentManager content)
        {
            bubbleTexture = content.Load<Texture2D>("Bubble");
        }

        public void Update(Viewport viewport)
        {
            if((bubbleRect.Y + bubbleRect.Height) <= 0)
            {
                bubbleRect.Y = viewport.Height;
                bubbleRect.Width = random.Next(50, 201);
                bubbleRect.Height = bubbleRect.Width;
                bubbleRect.X = random.Next(viewport.Width - bubbleRect.Width);
                bubbleSpeed = random.Next(1, 10);
            }
            bubbleRect.Y -= bubbleSpeed;
        }

        public void Draw(SpriteBatch spritebatch)
        {
            spritebatch.Draw(bubbleTexture, bubbleRect, Color.White);
        }

    }
}
