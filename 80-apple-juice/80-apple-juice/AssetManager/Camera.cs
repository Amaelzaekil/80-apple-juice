﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _80_apple_juice.AssetManager
{
    public class Camera 
    {
        //Attributes
        private Vector3 position;
        private Vector3 target;
        private Matrix cameraRotation;
        private float yaw, pitch, roll;
        private float speed;

        //Matrices
        public Matrix viewMatrix;
        public Matrix projectionMatrix;

        public Camera()
        {
            ResetCamera();
        }

        public void ResetCamera()
        {
            yaw = 0.0f;
            pitch = 0.0f;
            roll = 0.0f;
            speed = .3f;

            cameraRotation = Matrix.Identity;
            position = new Vector3(-40, 80, -40);
            target = new Vector3(15, 0, 15);

            viewMatrix = Matrix.Identity;
            projectionMatrix = Matrix.CreatePerspectiveFieldOfView(MathHelper.ToRadians(45.0f), 16 / 9, 0.5f, 500f);


        }

        public void Update()
        {
            HandleInput();
            UpdateViewMatrix();
        }


        private void HandleInput()
        {
            KeyboardState keyboardState = Keyboard.GetState();

            if (keyboardState.IsKeyDown(Keys.J))
            {
                yaw += .02f;
            }
            if (keyboardState.IsKeyDown(Keys.L))
            {
                yaw += -.02f;
            }
            if (keyboardState.IsKeyDown(Keys.K))
            {
                pitch += -.02f;
            }
            if (keyboardState.IsKeyDown(Keys.I))
            {
                pitch += .02f;
            }
            if (keyboardState.IsKeyDown(Keys.U))
            {
                roll += -.02f;
            }
            if (keyboardState.IsKeyDown(Keys.O))
            {
                roll += .02f;
            }

            if (keyboardState.IsKeyDown(Keys.W))
            {
                MoveCamera(cameraRotation.Forward);
            }
            if (keyboardState.IsKeyDown(Keys.S))
            {
                MoveCamera(-cameraRotation.Forward);
            }
            if (keyboardState.IsKeyDown(Keys.A))
            {
                MoveCamera(-cameraRotation.Right);
            }
            if (keyboardState.IsKeyDown(Keys.D))
            {
                MoveCamera(cameraRotation.Right);
            }
            if (keyboardState.IsKeyDown(Keys.E))
            {
                MoveCamera(cameraRotation.Up);
            }
            if (keyboardState.IsKeyDown(Keys.Q))
            {
                MoveCamera(-cameraRotation.Up);
            }
        }

        private void UpdateViewMatrix()
        {
            cameraRotation.Forward.Normalize();
            cameraRotation.Up.Normalize();
            cameraRotation.Right.Normalize();

            cameraRotation *= Matrix.CreateFromAxisAngle(cameraRotation.Right, pitch);
            cameraRotation *= Matrix.CreateFromAxisAngle(cameraRotation.Up, yaw);
            cameraRotation *= Matrix.CreateFromAxisAngle(cameraRotation.Forward, roll);

            yaw = 0.0f;
            pitch = 0.0f;
            roll = 0.0f;

            //target = position + cameraRotation.Forward;

            viewMatrix = Matrix.CreateLookAt(position, target, cameraRotation.Up);
        }

        private void MoveCamera(Vector3 addedVector)
        {
            position += speed * addedVector;
        }
    }
}