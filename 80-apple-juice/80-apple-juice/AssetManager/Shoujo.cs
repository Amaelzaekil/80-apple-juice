﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _80_apple_juice.AssetManager
{
    class Shoujo
    {
        #region Fields
        Model shoujo;
        BasicEffect effect;
        public Matrix platformWorld;
        public float health;
        #endregion

        public Shoujo()
        {
            health = 20;
        }

        public virtual void LoadContent(ContentManager content, GraphicsDevice graphicsDevice)
        {
            shoujo = content.Load<Model>("Shoujo");
            effect = new BasicEffect(graphicsDevice);
            effect.Projection = Matrix.CreateOrthographic(MathHelper.PiOver4,
                graphicsDevice.Viewport.AspectRatio, 1.0f, 100.0f);
            effect.View = Matrix.CreateLookAt(new Vector3(2, 3, 2), Vector3.Zero, Vector3.Up);
        }

        public virtual void Update()
        {
        }

        public virtual void Draw(GraphicsDevice graphicsDevice, Camera camera, Vector3 platformPos, float rotationY)
        {
            Matrix RotationMatrix;
            RotationMatrix = Matrix.CreateRotationY(rotationY);
            platformWorld = RotationMatrix * Matrix.CreateTranslation(platformPos);
            DrawModel(shoujo, platformWorld, camera);
        }

        public virtual void Draw(GraphicsDevice graphicsDevice, OrthographicCamera camera, Vector3 platformPos)
        {
            platformWorld = Matrix.CreateTranslation(platformPos);
            DrawModel(shoujo, platformWorld, camera);
        }

        private void DrawModel(Model model, Matrix world, Camera camera)
        {
            Matrix[] transforms = new Matrix[model.Bones.Count];
            model.CopyAbsoluteBoneTransformsTo(transforms);

            foreach (ModelMesh mesh in model.Meshes)
            {
                foreach (BasicEffect effect in mesh.Effects)
                {
                    effect.EnableDefaultLighting();
                    effect.World = transforms[mesh.ParentBone.Index] * world;

                    // Use the matrices provided by the chase camera
                    effect.View = camera.viewMatrix;
                    effect.Projection = camera.projectionMatrix;
                }
                mesh.Draw();
            }
        }

        private void DrawModel(Model model, Matrix world, OrthographicCamera camera)
        {
            Matrix[] transforms = new Matrix[model.Bones.Count];
            model.CopyAbsoluteBoneTransformsTo(transforms);

            foreach (ModelMesh mesh in model.Meshes)
            {
                foreach (BasicEffect effect in mesh.Effects)
                {
                    effect.EnableDefaultLighting();
                    effect.World = transforms[mesh.ParentBone.Index] * world;
                    

                    // Use the matrices provided by the chase camera
                    effect.View = camera.viewMatrix;
                    effect.Projection = camera.projectionMatrix;
                }
                mesh.Draw();
            }

        }
    }
}
