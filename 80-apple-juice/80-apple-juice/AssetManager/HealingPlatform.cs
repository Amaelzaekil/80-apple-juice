﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _80_apple_juice.AssetManager
{
    class HealingPlatform
    {
        #region Fields
        bool isPlayerTouching;
        Model healingPlatform;
        BasicEffect effect;
        Matrix hplatformWorld;
        #endregion

        public HealingPlatform()
        {
            isPlayerTouching = false;
        }

        public virtual void LoadContent(ContentManager content, GraphicsDevice graphicsDevice)
        {
            healingPlatform = content.Load<Model>("Platform");
            effect = new BasicEffect(graphicsDevice);
            effect.Projection = Matrix.CreateOrthographic(MathHelper.PiOver4,
                graphicsDevice.Viewport.AspectRatio, 1.0f, 100.0f);
            effect.View = Matrix.CreateLookAt(new Vector3(2, 3, 2), Vector3.Zero, Vector3.Up);
            effect.Texture = content.Load<Texture2D>("nPlatformTexture");
        }

        public virtual void Update(Shoujo player, Vector3 playerPos, Vector3 platformPos)
        {
            int count;
            count = 0;
            Vector3 offset = new Vector3(0.5f, 0, 0);
            if ((playerPos == (platformPos - offset)) || (playerPos == (platformPos + offset)) && count == 0)
            {
                isPlayerTouching = true;
            }

            if (isPlayerTouching == true)
            {
                player.health += 5;
                count++;
            }
            if (count == 1)
            {
                player.health -= 0;
            }
            if (isPlayerTouching == false) 
            {
                count = 0;
            }
        }

        public virtual void Draw(GraphicsDevice graphicsDevice, Camera camera, Vector3 platformPos)
        {
            hplatformWorld = Matrix.CreateTranslation(platformPos);
            DrawModel(healingPlatform, hplatformWorld, camera);
        }

        public virtual void Draw(GraphicsDevice graphicsDevice, OrthographicCamera camera, Vector3 platformPos)
        {
            hplatformWorld = Matrix.CreateTranslation(platformPos);
            DrawModel(healingPlatform, hplatformWorld, camera);
        }

        private void DrawModel(Model model, Matrix world, Camera camera)
        {
            Matrix[] transforms = new Matrix[model.Bones.Count];
            model.CopyAbsoluteBoneTransformsTo(transforms);

            foreach (ModelMesh mesh in model.Meshes)
            {
                foreach (BasicEffect effect in mesh.Effects)
                {
                    effect.EnableDefaultLighting();
                    effect.World = transforms[mesh.ParentBone.Index] * world;

                    // Use the matrices provided by the chase camera
                    effect.View = camera.viewMatrix;
                    effect.Projection = camera.projectionMatrix;
                }
                mesh.Draw();
            }
        }

        private void DrawModel(Model model, Matrix world, OrthographicCamera camera)
        {
            Matrix[] transforms = new Matrix[model.Bones.Count];
            model.CopyAbsoluteBoneTransformsTo(transforms);

            foreach (ModelMesh mesh in model.Meshes)
            {
                foreach (BasicEffect effect in mesh.Effects)
                {
                    effect.EnableDefaultLighting();
                    effect.World = transforms[mesh.ParentBone.Index] * world;
                    

                    // Use the matrices provided by the chase camera
                    effect.View = camera.viewMatrix;
                    effect.Projection = camera.projectionMatrix;
                }
                mesh.Draw();
            }

        }
    }
}
