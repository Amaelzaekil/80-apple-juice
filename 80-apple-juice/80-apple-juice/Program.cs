using System;

namespace _80_apple_juice
{
#if WINDOWS || XBOX
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            using (theGame game = new theGame())
            {
                game.Run();
            }
        }
    }
#endif
}

