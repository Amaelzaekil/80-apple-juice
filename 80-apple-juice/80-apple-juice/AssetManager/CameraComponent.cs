using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace GameStateManagement
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class CameraComponent : Microsoft.Xna.Framework.GameComponent
    {
        protected Matrix view;
        protected Matrix proj;
        public CameraComponent(Game game)
            : base(game)
        {
            // TODO: Construct any child components here
        }

        public Matrix View
        {
            get
            {
                return view;
            }
        }
        public Matrix Projection
        {
            get
            {
                return proj;
            }
        }
        /// <summary>
        /// Allows the game component to perform any initialization it needs to before starting
        /// to run.  This is where it can query for any required services and load content.
        /// </summary>
        public override void Initialize(Vector3 camPos, Vector3 camTgt)
        {
            // TODO: Add your initialization code here

            view = Matrix.CreateLookAt(camPos, camTgt, Vector3.Up);
            proj = Matrix.CreatePerspectiveFieldOfView(MathHelper.PiOver4, 
                Game.GraphicsDevice.Viewport.AspectRatio, 1.0f, 100.0f);
            base.Initialize();
        }

        /// <summary>
        /// Allows the game component to update itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime)
        {
            // TODO: Add your update code here

            base.Update(gameTime);
        }
    }
}
