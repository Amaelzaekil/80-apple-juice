﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _80_apple_juice.AssetManager
{
    class Platform
    {
        #region Fields
        //bool isPlayerTouching;
        Model platform;
        //public Effect effect;

        public BasicEffect effect;

        Matrix platformWorld;
        Texture2D platformTex;
        
        #endregion

        public Platform()
        {
            //isPlayerTouching = false;
        }

        public virtual void LoadContent(ContentManager content, GraphicsDevice graphicsDevice)
        {
            effect = new BasicEffect(graphicsDevice);
            platform = content.Load<Model>("Platform");
            //effect = content.Load<Effect>("Toon");

            effect.Projection = Matrix.CreateOrthographic(MathHelper.PiOver4,
                graphicsDevice.Viewport.AspectRatio, 1.0f, 100.0f);
            effect.View = Matrix.CreateLookAt(new Vector3(2, 3, 2), Vector3.Zero, Vector3.Up);
            effect.Texture = content.Load<Texture2D>("nPlatformTexture");

            //effect.Projection = Matrix.CreateOrthographic(MathHelper.PiOver4,
            //    graphicsDevice.Viewport.AspectRatio, 1.0f, 100.0f);
            //effect.View = Matrix.CreateLookAt(new Vector3(2, 3, 2), Vector3.Zero, Vector3.Up);
            //effect.Texture = content.Load<Texture2D>("nPlatformTexture");

            platformTex = content.Load<Texture2D>("nPlatformTexture");
        }

        public virtual void Update()
        {
            
        }

        public virtual void Draw(GraphicsDevice graphicsDevice, Camera camera, Vector3 platformPos)
        {
            platformWorld = Matrix.CreateTranslation(platformPos);
            DrawModel(platform, platformWorld, camera);
        }

        public virtual void Draw(GraphicsDevice graphicsDevice, OrthographicCamera camera, Vector3 platformPos)
        {
            platformWorld = Matrix.CreateTranslation(platformPos);
            DrawModel(platform, platformWorld, camera);
        }

        private void DrawModel(Model model, Matrix world, Camera camera)
        {
            Matrix[] transforms = new Matrix[model.Bones.Count];
            model.CopyAbsoluteBoneTransformsTo(transforms);

            foreach (ModelMesh mesh in model.Meshes)
            {
                foreach (BasicEffect effect in mesh.Effects)
                {
                    effect.EnableDefaultLighting();
                    effect.PreferPerPixelLighting = true;
                    effect.World = transforms[mesh.ParentBone.Index] * world;

                    // Use the matrices provided by the chase camera
                    effect.View = camera.viewMatrix;
                    effect.Projection = camera.projectionMatrix;
                }
                mesh.Draw();
            }
        }

        private void DrawModel(Model model, Matrix world, OrthographicCamera camera)
        {
            Matrix[] transforms = new Matrix[model.Bones.Count];
            model.CopyAbsoluteBoneTransformsTo(transforms);

            foreach (ModelMesh mesh in model.Meshes)
            {
                foreach (BasicEffect effect in mesh.Effects)
                {
                    effect.EnableDefaultLighting();
                    effect.PreferPerPixelLighting = true;
                    effect.World = transforms[mesh.ParentBone.Index] * world;
                    

                    // Use the matrices provided by the chase camera
                    effect.View = camera.viewMatrix;
                    effect.Projection = camera.projectionMatrix;
                }
                mesh.Draw();
            }

        }

        //private void DrawModelWithEffect(Model model, Matrix world, Camera camera)
        //{
        //    Matrix[] transforms = new Matrix[model.Bones.Count];
        //    model.CopyAbsoluteBoneTransformsTo(transforms);


        //    foreach (ModelMesh mesh in model.Meshes)
        //    {
        //        foreach (ModelMeshPart part in mesh.MeshParts)
        //        {
                    
        //            effect.Parameters["World"].SetValue(transforms[mesh.ParentBone.Index] * world);
        //            effect.Parameters["View"].SetValue(camera.viewMatrix);
        //            effect.Parameters["Projection"].SetValue(camera.projectionMatrix);
        //            effect.Parameters["WorldInverseTranspose"].SetValue(
        //                                    Matrix.Transpose(Matrix.Invert(transforms[mesh.ParentBone.Index] * world)));
        //            effect.Parameters["Texture"].SetValue(platformTex);
        //            part.Effect = effect;
        //        }
        //        mesh.Draw();
        //    }
        //}

        //private void DrawModelWithEffect(Model model, Matrix world, OrthographicCamera camera)
        //{
        //    Matrix[] transforms = new Matrix[model.Bones.Count];
        //    model.CopyAbsoluteBoneTransformsTo(transforms);

        //    foreach (ModelMesh mesh in model.Meshes)
        //    {
        //        foreach (ModelMeshPart part in mesh.MeshParts)
        //        {
                    
        //            effect.Parameters["World"].SetValue(transforms[mesh.ParentBone.Index] * world);
        //            effect.Parameters["View"].SetValue(camera.viewMatrix);
        //            effect.Parameters["Projection"].SetValue(camera.projectionMatrix);
        //            effect.Parameters["WorldInverseTranspose"].SetValue(
        //                                    Matrix.Transpose(Matrix.Invert(transforms[mesh.ParentBone.Index] * world)));
        //            effect.Parameters["Texture"].SetValue(platformTex);
        //            part.Effect = effect;
        //        }
        //        mesh.Draw();
        //    }

        //}
        private void DrawModelWithEffect(Model model, Matrix world, Camera camera)
        {
            Matrix[] transforms = new Matrix[model.Bones.Count];
            model.CopyAbsoluteBoneTransformsTo(transforms);
            foreach (ModelMesh mesh in model.Meshes)
            {
                foreach (ModelMeshPart part in mesh.MeshParts)
                {
                    //part.Effect = effect;
                    //effect.Parameters["World"].SetValue(transforms[mesh.ParentBone.Index] * world);
                    //effect.Parameters["View"].SetValue(camera.viewMatrix);
                    //effect.Parameters["Projection"].SetValue(camera.projectionMatrix);
                    //effect.Parameters["WorldInverseTranspose"].SetValue(
                    //                        Matrix.Transpose(Matrix.Invert(transforms[mesh.ParentBone.Index] * world)));
                    //effect.Parameters["Texture"].SetValue(platformTex);
                }
                mesh.Draw();
            }
        }

        private void DrawModelWithEffect(Model model, Matrix world, OrthographicCamera camera)
        {
            Matrix[] transforms = new Matrix[model.Bones.Count];
            model.CopyAbsoluteBoneTransformsTo(transforms);

            foreach (ModelMesh mesh in model.Meshes)
            {
                foreach (ModelMeshPart part in mesh.MeshParts)
                {
                    //part.Effect = effect;
                    //effect.Parameters["World"].SetValue(transforms[mesh.ParentBone.Index] * world);
                    //effect.Parameters["View"].SetValue(camera.viewMatrix);
                    //effect.Parameters["Projection"].SetValue(camera.projectionMatrix);
                    //effect.Parameters["WorldInverseTranspose"].SetValue(
                    //                        Matrix.Transpose(Matrix.Invert(transforms[mesh.ParentBone.Index] * world)));
                    //effect.Parameters["Texture"].SetValue(platformTex);
                }
                mesh.Draw();
            }

        }

    }
}
